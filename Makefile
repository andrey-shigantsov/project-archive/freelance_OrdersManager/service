TARGET = omService

CXXFLAGS += -m32 -std=c++11

LIBS_PATH = -Lmysql/
LIBS = -lstdc++ -lmysqlclient

GSOAP_PATH = etc/gsoap
GSOAP_SRC = ${GSOAP_PATH}/stdsoap2.cpp
GSOAP_GEN_SRC = ${GSOAP_PATH}/gen/soapC.cpp ${GSOAP_PATH}/gen/soapOrdersManagerBindingService.cpp

SRC = main.cpp ${GSOAP_SRC} ${GSOAP_GEN_SRC}

.PHONY: all debug clean

all: ${TARGET} debug

debug: DEFINES += -DLOCAL_SERVER
debug: CXXFLAGS += -DDEBUG -ggdb3
debug: ${TARGET}

${TARGET}: ${SRC}
	${CC} ${CXXFLAGS} ${DEFINES} ${LIBS} -o ${TARGET} ${SRC}
	
clean:
	rm ${TARGET}
