/* soapStub.h
   Generated by gSOAP 2.8.22 from OrdersManager.h

Copyright(C) 2000-2015, Robert van Engelen, Genivia Inc. All Rights Reserved.
The generated code is released under one of the following licenses:
GPL or Genivia's license for commercial use.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
*/

#ifndef soapStub_H
#define soapStub_H
#include <vector>
#define SOAP_NAMESPACE_OF_ns1	"http://www.ordersmanager.ru/"
#include "stdsoap2.h"
#if GSOAP_VERSION != 20822
# error "GSOAP VERSION 20822 MISMATCH IN GENERATED CODE VERSUS LIBRARY CODE: PLEASE REINSTALL PACKAGE"
#endif


/******************************************************************************\
 *                                                                            *
 * Enumerations                                                               *
 *                                                                            *
\******************************************************************************/


#ifndef SOAP_TYPE_ns1__responseCodeType
#define SOAP_TYPE_ns1__responseCodeType (15)
/* ns1:responseCodeType */
enum ns1__responseCodeType { ns1__responseCodeType__Success = 0, ns1__responseCodeType__Failure = 1, ns1__responseCodeType__EmailFailure = 2, ns1__responseCodeType__PasswordFailure = 3, ns1__responseCodeType__FirstnameFailure = 4, ns1__responseCodeType__PatronymicFailure = 5, ns1__responseCodeType__LastnameFailure = 6 };
typedef enum ns1__responseCodeType ns1__responseCodeType;
#endif

/******************************************************************************\
 *                                                                            *
 * Types with Custom Serializers                                              *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Classes and Structs                                                        *
 *                                                                            *
\******************************************************************************/


#if 0 /* volatile type: do not declare here, declared elsewhere */

#endif

#if 0 /* volatile type: do not declare here, declared elsewhere */

#endif

#ifndef SOAP_TYPE_ns1__clientAuthorizationInfoType
#define SOAP_TYPE_ns1__clientAuthorizationInfoType (8)
/* ns1:clientAuthorizationInfoType */
class SOAP_CMAC ns1__clientAuthorizationInfoType
{
public:
	std::string email;	/* required element of type xsd:string */
	std::string passwd;	/* required element of type xsd:string */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 8; } /* = unique type id SOAP_TYPE_ns1__clientAuthorizationInfoType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__clientAuthorizationInfoType() { ns1__clientAuthorizationInfoType::soap_default(NULL); }
	virtual ~ns1__clientAuthorizationInfoType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__clientBaseInfoType
#define SOAP_TYPE_ns1__clientBaseInfoType (9)
/* ns1:clientBaseInfoType */
class SOAP_CMAC ns1__clientBaseInfoType
{
public:
	std::string FirstName;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type xsd:string */
	std::string LastName;	/* required element of type xsd:string */
	std::string Patronymic;	/* required element of type xsd:string */
	std::string phone;	/* required element of type xsd:string */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 9; } /* = unique type id SOAP_TYPE_ns1__clientBaseInfoType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__clientBaseInfoType() { ns1__clientBaseInfoType::soap_default(NULL); }
	virtual ~ns1__clientBaseInfoType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__clientFullInfoType
#define SOAP_TYPE_ns1__clientFullInfoType (10)
/* ns1:clientFullInfoType */
class SOAP_CMAC ns1__clientFullInfoType
{
public:
	ns1__clientAuthorizationInfoType *authInfo;	/* required element of type ns1:clientAuthorizationInfoType */
	ns1__clientBaseInfoType *baseInfo;	/* required element of type ns1:clientBaseInfoType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 10; } /* = unique type id SOAP_TYPE_ns1__clientFullInfoType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__clientFullInfoType() { ns1__clientFullInfoType::soap_default(NULL); }
	virtual ~ns1__clientFullInfoType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__OrderType
#define SOAP_TYPE_ns1__OrderType (11)
/* ns1:OrderType */
class SOAP_CMAC ns1__OrderType
{
public:
	int id;	/* required element of type xsd:int */
	int idDoer;	/* required element of type xsd:int */
	int idOwner;	/* required element of type xsd:int */
	int idDestination;	/* required element of type xsd:int */
	int idBusiness;	/* required element of type xsd:int */
	std::string Header;	/* required element of type xsd:string */
	std::string Description;	/* required element of type xsd:string */
	std::string Price;	/* required element of type xsd:string */
	std::string Deadline;	/* required element of type xsd:string */
	bool isFinished;	/* required element of type xsd:boolean */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 11; } /* = unique type id SOAP_TYPE_ns1__OrderType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__OrderType() { ns1__OrderType::soap_default(NULL); }
	virtual ~ns1__OrderType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__idListType
#define SOAP_TYPE_ns1__idListType (12)
/* ns1:idListType */
class SOAP_CMAC ns1__idListType
{
public:
	std::vector<int >id;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* optional element of type xsd:int */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 12; } /* = unique type id SOAP_TYPE_ns1__idListType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__idListType() { ns1__idListType::soap_default(NULL); }
	virtual ~ns1__idListType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__OrdersListType
#define SOAP_TYPE_ns1__OrdersListType (13)
/* ns1:OrdersListType */
class SOAP_CMAC ns1__OrdersListType
{
public:
	std::vector<ns1__OrderType * >data;	/* optional element of type ns1:OrderType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 13; } /* = unique type id SOAP_TYPE_ns1__OrdersListType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__OrdersListType() { ns1__OrdersListType::soap_default(NULL); }
	virtual ~ns1__OrdersListType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__OrdersQueryType
#define SOAP_TYPE_ns1__OrdersQueryType (14)
/* ns1:OrdersQueryType */
class SOAP_CMAC ns1__OrdersQueryType
{
public:
	int id;	/* required element of type xsd:int */
	int idDoer;	/* required element of type xsd:int */
	int idOwner;	/* required element of type xsd:int */
	int idDestination;	/* required element of type xsd:int */
	int idBusiness;	/* required element of type xsd:int */
	bool isFinished;	/* required element of type xsd:boolean */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 14; } /* = unique type id SOAP_TYPE_ns1__OrdersQueryType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__OrdersQueryType() { ns1__OrdersQueryType::soap_default(NULL); }
	virtual ~ns1__OrdersQueryType() { }
};
#endif

#ifndef SOAP_TYPE___ns1__GetResponseCodeStringResponse
#define SOAP_TYPE___ns1__GetResponseCodeStringResponse (26)
/* Operation wrapper: */
struct __ns1__GetResponseCodeStringResponse
{
public:
	std::string str;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type xsd:string */
public:
	int soap_type() const { return 26; } /* = unique type id SOAP_TYPE___ns1__GetResponseCodeStringResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__GetResponseCodeString
#define SOAP_TYPE___ns1__GetResponseCodeString (27)
/* Operation wrapper: */
struct __ns1__GetResponseCodeString
{
public:
	enum ns1__responseCodeType code;	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 27; } /* = unique type id SOAP_TYPE___ns1__GetResponseCodeString */
};
#endif

#ifndef SOAP_TYPE___ns1__GetUserIdResponse
#define SOAP_TYPE___ns1__GetUserIdResponse (30)
/* Operation wrapper: */
struct __ns1__GetUserIdResponse
{
public:
	int value;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type xsd:int */
public:
	int soap_type() const { return 30; } /* = unique type id SOAP_TYPE___ns1__GetUserIdResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__GetUserId
#define SOAP_TYPE___ns1__GetUserId (31)
/* Operation wrapper: */
struct __ns1__GetUserId
{
public:
	std::string str;	/* required element of type xsd:string */
public:
	int soap_type() const { return 31; } /* = unique type id SOAP_TYPE___ns1__GetUserId */
};
#endif

#ifndef SOAP_TYPE___ns1__GetBusinessNameResponse
#define SOAP_TYPE___ns1__GetBusinessNameResponse (33)
/* Operation wrapper: */
struct __ns1__GetBusinessNameResponse
{
public:
	std::string str;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type xsd:string */
public:
	int soap_type() const { return 33; } /* = unique type id SOAP_TYPE___ns1__GetBusinessNameResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__GetBusinessName
#define SOAP_TYPE___ns1__GetBusinessName (34)
/* Operation wrapper: */
struct __ns1__GetBusinessName
{
public:
	int idBusiness;	/* required element of type xsd:int */
public:
	int soap_type() const { return 34; } /* = unique type id SOAP_TYPE___ns1__GetBusinessName */
};
#endif

#ifndef SOAP_TYPE___ns1__GetIdBusinessList
#define SOAP_TYPE___ns1__GetIdBusinessList (37)
/* Operation wrapper: */
struct __ns1__GetIdBusinessList
{
public:
	int idParent;	/* required element of type xsd:int */
public:
	int soap_type() const { return 37; } /* = unique type id SOAP_TYPE___ns1__GetIdBusinessList */
};
#endif

#ifndef SOAP_TYPE___ns1__GetUserInfo
#define SOAP_TYPE___ns1__GetUserInfo (40)
/* Operation wrapper: */
struct __ns1__GetUserInfo
{
public:
	int idUser;	/* required element of type xsd:int */
public:
	int soap_type() const { return 40; } /* = unique type id SOAP_TYPE___ns1__GetUserInfo */
};
#endif

#ifndef SOAP_TYPE___ns1__RegistrationResponse
#define SOAP_TYPE___ns1__RegistrationResponse (44)
/* Operation wrapper: */
struct __ns1__RegistrationResponse
{
public:
	enum ns1__responseCodeType code;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 44; } /* = unique type id SOAP_TYPE___ns1__RegistrationResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__Registration
#define SOAP_TYPE___ns1__Registration (45)
/* Operation wrapper: */
struct __ns1__Registration
{
public:
	ns1__clientFullInfoType *info;	/* optional element of type ns1:clientFullInfoType */
public:
	int soap_type() const { return 45; } /* = unique type id SOAP_TYPE___ns1__Registration */
};
#endif

#ifndef SOAP_TYPE___ns1__AuthorizationResponse
#define SOAP_TYPE___ns1__AuthorizationResponse (47)
/* Operation wrapper: */
struct __ns1__AuthorizationResponse
{
public:
	enum ns1__responseCodeType code;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 47; } /* = unique type id SOAP_TYPE___ns1__AuthorizationResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__Authorization
#define SOAP_TYPE___ns1__Authorization (48)
/* Operation wrapper: */
struct __ns1__Authorization
{
public:
	ns1__clientAuthorizationInfoType *authInfo;	/* optional element of type ns1:clientAuthorizationInfoType */
public:
	int soap_type() const { return 48; } /* = unique type id SOAP_TYPE___ns1__Authorization */
};
#endif

#ifndef SOAP_TYPE___ns1__SendOrderResponse
#define SOAP_TYPE___ns1__SendOrderResponse (50)
/* Operation wrapper: */
struct __ns1__SendOrderResponse
{
public:
	enum ns1__responseCodeType code;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 50; } /* = unique type id SOAP_TYPE___ns1__SendOrderResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__SendOrder
#define SOAP_TYPE___ns1__SendOrder (51)
/* Operation wrapper: */
struct __ns1__SendOrder
{
public:
	ns1__OrderType *order;	/* optional element of type ns1:OrderType */
	ns1__clientAuthorizationInfoType *auth;	/* optional element of type ns1:clientAuthorizationInfoType */
public:
	int soap_type() const { return 51; } /* = unique type id SOAP_TYPE___ns1__SendOrder */
};
#endif

#ifndef SOAP_TYPE___ns1__TakeOrderResponse
#define SOAP_TYPE___ns1__TakeOrderResponse (53)
/* Operation wrapper: */
struct __ns1__TakeOrderResponse
{
public:
	enum ns1__responseCodeType code;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 53; } /* = unique type id SOAP_TYPE___ns1__TakeOrderResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__TakeOrder
#define SOAP_TYPE___ns1__TakeOrder (54)
/* Operation wrapper: */
struct __ns1__TakeOrder
{
public:
	int takeId;	/* required element of type xsd:int */
	ns1__clientAuthorizationInfoType *auth;	/* optional element of type ns1:clientAuthorizationInfoType */
public:
	int soap_type() const { return 54; } /* = unique type id SOAP_TYPE___ns1__TakeOrder */
};
#endif

#ifndef SOAP_TYPE___ns1__FinishOrderResponse
#define SOAP_TYPE___ns1__FinishOrderResponse (56)
/* Operation wrapper: */
struct __ns1__FinishOrderResponse
{
public:
	enum ns1__responseCodeType code;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 56; } /* = unique type id SOAP_TYPE___ns1__FinishOrderResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__FinishOrder
#define SOAP_TYPE___ns1__FinishOrder (57)
/* Operation wrapper: */
struct __ns1__FinishOrder
{
public:
	int finishId;	/* required element of type xsd:int */
	ns1__clientAuthorizationInfoType *auth;	/* optional element of type ns1:clientAuthorizationInfoType */
public:
	int soap_type() const { return 57; } /* = unique type id SOAP_TYPE___ns1__FinishOrder */
};
#endif

#ifndef SOAP_TYPE___ns1__FindOrdersResponse
#define SOAP_TYPE___ns1__FindOrdersResponse (58)
/* Operation wrapper: */
struct __ns1__FindOrdersResponse
{
public:
	ns1__OrdersListType *orders;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* optional element of type ns1:OrdersListType */
	enum ns1__responseCodeType code;	/* required element of type ns1:responseCodeType */
public:
	int soap_type() const { return 58; } /* = unique type id SOAP_TYPE___ns1__FindOrdersResponse */
};
#endif

#ifndef SOAP_TYPE___ns1__FindOrders
#define SOAP_TYPE___ns1__FindOrders (63)
/* Operation wrapper: */
struct __ns1__FindOrders
{
public:
	ns1__OrdersQueryType *query;	/* optional element of type ns1:OrdersQueryType */
	ns1__clientAuthorizationInfoType *auth;	/* optional element of type ns1:clientAuthorizationInfoType */
public:
	int soap_type() const { return 63; } /* = unique type id SOAP_TYPE___ns1__FindOrders */
};
#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Header
#define SOAP_TYPE_SOAP_ENV__Header (64)
/* SOAP Header: */
struct SOAP_ENV__Header
{
public:
	int soap_type() const { return 64; } /* = unique type id SOAP_TYPE_SOAP_ENV__Header */
};
#endif

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Code
#define SOAP_TYPE_SOAP_ENV__Code (65)
/* SOAP Fault Code: */
struct SOAP_ENV__Code
{
public:
	char *SOAP_ENV__Value;	/* optional element of type xsd:QName */
	struct SOAP_ENV__Code *SOAP_ENV__Subcode;	/* optional element of type SOAP-ENV:Code */
public:
	int soap_type() const { return 65; } /* = unique type id SOAP_TYPE_SOAP_ENV__Code */
};
#endif

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Detail
#define SOAP_TYPE_SOAP_ENV__Detail (67)
/* SOAP-ENV:Detail */
struct SOAP_ENV__Detail
{
public:
	char *__any;
	int __type;	/* any type of element <fault> (defined below) */
	void *fault;	/* transient */
public:
	int soap_type() const { return 67; } /* = unique type id SOAP_TYPE_SOAP_ENV__Detail */
};
#endif

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Reason
#define SOAP_TYPE_SOAP_ENV__Reason (70)
/* SOAP-ENV:Reason */
struct SOAP_ENV__Reason
{
public:
	char *SOAP_ENV__Text;	/* optional element of type xsd:string */
public:
	int soap_type() const { return 70; } /* = unique type id SOAP_TYPE_SOAP_ENV__Reason */
};
#endif

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Fault
#define SOAP_TYPE_SOAP_ENV__Fault (71)
/* SOAP Fault: */
struct SOAP_ENV__Fault
{
public:
	char *faultcode;	/* optional element of type xsd:QName */
	char *faultstring;	/* optional element of type xsd:string */
	char *faultactor;	/* optional element of type xsd:string */
	struct SOAP_ENV__Detail *detail;	/* optional element of type SOAP-ENV:Detail */
	struct SOAP_ENV__Code *SOAP_ENV__Code;	/* optional element of type SOAP-ENV:Code */
	struct SOAP_ENV__Reason *SOAP_ENV__Reason;	/* optional element of type SOAP-ENV:Reason */
	char *SOAP_ENV__Node;	/* optional element of type xsd:string */
	char *SOAP_ENV__Role;	/* optional element of type xsd:string */
	struct SOAP_ENV__Detail *SOAP_ENV__Detail;	/* optional element of type SOAP-ENV:Detail */
public:
	int soap_type() const { return 71; } /* = unique type id SOAP_TYPE_SOAP_ENV__Fault */
};
#endif

#endif

/******************************************************************************\
 *                                                                            *
 * Typedefs                                                                   *
 *                                                                            *
\******************************************************************************/

#ifndef SOAP_TYPE__QName
#define SOAP_TYPE__QName (5)
typedef char *_QName;
#endif

#ifndef SOAP_TYPE__XML
#define SOAP_TYPE__XML (6)
typedef char *_XML;
#endif


/******************************************************************************\
 *                                                                            *
 * Externals                                                                  *
 *                                                                            *
\******************************************************************************/


#endif

/* End of soapStub.h */
