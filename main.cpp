/*
 * main.cpp
 *
 *  Created on: 30 авг. 2015 г.
 *      Author: svetozar
 */

#include "etc/gsoap/gen/OrdersManagerBinding.nsmap"
#include "etc/gsoap/gen/soapOrdersManagerBindingService.h"

#include <mysql/mysql.h>

#include <cstring>
#include <string>

static MYSQL *db;
static ns1__OrdersListType OrdersList;

static ns1__OrderType* newOrder()
{
  ns1__OrderType* o = new ns1__OrderType();
  OrdersList.data.push_back(o);
  return o;
}
static void clearOrdersList()
{
  for (int i = 0; i < OrdersList.data.size(); i++)
    if (OrdersList.data[i] != NULL)
      delete OrdersList.data[i];
  OrdersList.data.clear();
}

static inline void db_error_handler(const char* ownerName)
{
  fprintf(stderr, "%s: db error: %s\n", ownerName, mysql_error(db));
}

void close_all()
{
  clearOrdersList();
  
  mysql_close(db);
}

#ifdef LOCAL_SERVER
#define DB_URL "192.168.0.2"
#else
#define DB_URL "localhost"
#endif

int main()
{  
  db = mysql_init(NULL);
  if (db == NULL) 
  {
    db_error_handler("Init");
    return -1;
  }
  if (mysql_real_connect(db, DB_URL, "omserver", "omserver", NULL, 0, NULL, 0) == NULL) 
  {
    db_error_handler("Init");
    close_all();
    return -1;
  }
  if (mysql_options(db, MYSQL_SET_CHARSET_NAME, "utf8"))
    db_error_handler("Init");
  
  OrdersManagerBindingService s(SOAP_XML_INDENT);
#ifdef LOCAL_SERVER
  if (s.run(8080) != SOAP_OK)
#else
  if (s.serve() != SOAP_OK)
#endif
  {
    s.soap_print_fault(stderr);
  }
  s.destroy();

  close_all();
  return 0;
}

/////////////////////////////////////////////////////////////////////

bool db_field_id(char* field, int* value)
{
  *value = -1;
  if (field == NULL)
    return false;
  if (strstr(field, "null") != NULL)
  {
    return false;
  }
  if (sscanf(field, "%d", value) != 1)
    return false;
  return true;
}
bool db_field_int(char* field, int* value)
{
  *value = 0;
  if (field == NULL)
    return false;
  if (strstr(field, "null") != NULL)
  {
    return false;
  }
  if (sscanf(field, "%d", value) != 1)
    return false;
  return true;
}
bool db_field_bool(char* field, bool* value)
{
  int tmp = 0;
  bool res = db_field_int(field, &tmp);
  *value = tmp;
  return res;
}
bool db_field_string(char* field, std::string* value)
{
  *value = "";
  if (field == NULL)
    return false;
  *value = field;
  return true;
}

bool db_count_res(std::string& query, long* count)
{
  MYSQL_STMT *stmt;
  MYSQL_BIND bind;
  my_bool error;

  stmt = mysql_stmt_init(db);
  if (stmt == NULL)
  {
    db_error_handler("Count");
    return false;
  }
  if (mysql_stmt_prepare(stmt, query.c_str(), query.size()))
  {
    db_error_handler("Count");
    return false;
  }
  if(mysql_stmt_execute(stmt))
  {
    db_error_handler("Count");
    return false;
  }
  
  memset(&bind, 0, sizeof(bind));
  bind.buffer_type = MYSQL_TYPE_LONG;
  bind.buffer = (char *)count;
  bind.error = &error;
  
  if(mysql_stmt_bind_result(stmt, &bind))
  {
    db_error_handler("Count");
    mysql_stmt_close(stmt);
    return false;
  }
  if(mysql_stmt_fetch(stmt))
  {
    db_error_handler("Count");
    mysql_stmt_close(stmt);
    return false;
  }
  mysql_stmt_close(stmt);
  return true;
}

inline std::string db_value_id(int val)
{
  return (val == -1)?("NULL"):("'" + std::to_string(val) + "'");
}

bool check_email(std::string email)
{
  std::string query = "SELECT COUNT(*) FROM `omserver`.`users` WHERE `email`='" + email + "';";
  long count = -1;
  if(!db_count_res(query, &count))
    return false;
  bool res = (count == 1);
  return res;
}

bool check_password(std::string email, std::string passwd)
{
  std::string query = "SELECT `password` FROM `omserver`.`users` WHERE `email`='" + email + "';";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("Passwd");
    return false;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("Passwd");
    return false;
  }
  if(mysql_num_fields(resStore) != 1)
  {
    fprintf(stderr, "Passwd: error: invalid num_fields\n");
    return false;
  }
  MYSQL_ROW row = mysql_fetch_row(resStore);
  bool res = (passwd == row[0]);  
  mysql_free_result(resStore);
  return res;
}

int UserId(std::string email)
{
  std::string query = "SELECT `id` FROM `omserver`.`users` "
                      "WHERE `email` LIKE '" + email + "';";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("GetUserId");
    return -1;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("GetUserId: store");
    return -1;
  }

  MYSQL_ROW row;
  row = mysql_fetch_row(resStore);
  if (!row)
  {
    return -1;
  }

  int value = -1;
  sscanf(row[0], "%d", &value);

  mysql_free_result(resStore);

  return value;
}

int OrdersManagerBindingService::GetResponseCodeString(ns1__responseCodeType code, std::string &codeString)
{
  switch(code)
  {
  case ns1__responseCodeType__Success:
    codeString = "Success";
    break;
	
  case ns1__responseCodeType__Failure:
    codeString = "Failure";
    break;
	
  case ns1__responseCodeType__EmailFailure:
    codeString = "Email failure";
    break;
	
  case ns1__responseCodeType__PasswordFailure:
    codeString = "Password Failure";
    break;
	
  case ns1__responseCodeType__FirstnameFailure:
    codeString = "Firstname failure";
    break;
	
  case ns1__responseCodeType__PatronymicFailure:
    codeString = "Patronymic faiure";
    break;
	
  case ns1__responseCodeType__LastnameFailure:
    codeString = "Lastname failure";
    break;
  }
  return SOAP_OK;
}

int OrdersManagerBindingService::GetUserId(std::string str, int &value)
{
  value = UserId(str);
  return SOAP_OK;
}

static bool build_businessName(int idBusiness, std::string &str)
{
  std::string basic_query = "SELECT `name`,`idParent` FROM `omserver`.`businesses` WHERE `id` = ",
              query = basic_query + std::to_string(idBusiness);
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("GetBusinessName");
    return false;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("GetBusinessName: store");
    return false;
  }

  MYSQL_ROW row;
  row = mysql_fetch_row(resStore);
  if (!row)
  {
    mysql_free_result(resStore);
    return false;
  }
  std::string base = (str != "")?("::" + str):("");
  str = row[0] + base;

  int idParent = -1;
  db_field_int(row[1], &idParent);
  mysql_free_result(resStore);

  build_businessName(idParent, str);
  return true;
}

int OrdersManagerBindingService::GetBusinessName(int idBusiness, std::string &str)
{
  str = "";
  build_businessName(idBusiness, str);
  return SOAP_OK;
}

int OrdersManagerBindingService::GetIdBusinessList(int idParent, ns1__idListType &idBussinessList)
{
  std::string query = "SELECT `id` FROM `omserver`.`businesses` "
                      "WHERE `idParent` ";
  if (idParent == -1)
    query += "is NULL";
  else
    query += "= " + db_value_id(idParent);
  query += ";";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("GetIdBusinessList");
    return SOAP_OK;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("GetIdBusinessList: store");
    return false;
  }

  MYSQL_ROW row;
  while(row = mysql_fetch_row(resStore))
  {
    int id = 0;
    db_field_id(row[0], &id);
    idBussinessList.id.push_back(id);
  }

  mysql_free_result(resStore);

  return SOAP_OK;
}

int OrdersManagerBindingService::GetUserInfo(int idUser, ns1__clientBaseInfoType &userInfo)
{
  std::string query = "SELECT `Firstname`, `Patronymic`, `Lastname`, `phone` FROM `omserver`.`users` "
                      "WHERE `id` = " + std::to_string(idUser) + ";";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("GetOwnerName");
    return SOAP_OK;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("GetOwnerName: store");
    return false;
  }

  MYSQL_ROW row;
  row = mysql_fetch_row(resStore);
  if (!row)
  {
    return SOAP_OK;
  }

  userInfo.FirstName = row[0];
  userInfo.Patronymic = row[1];
  userInfo.LastName = row[2];
  userInfo.phone = row[3];

  mysql_free_result(resStore);

  return SOAP_OK;
}

int OrdersManagerBindingService::Registration(ns1__clientFullInfoType *client, ns1__responseCodeType &code)
{
  if (check_email(client->authInfo->email))
  {
    code = ns1__responseCodeType__EmailFailure;
    return SOAP_OK;
  }  
  
  std::string query = "INSERT INTO `omserver`.`users` "
    "(`id`, `email`, `password`, `Firstname`, `Patronymic`, `Lastname`, `phone`) "
	  "VALUES (NULL"
	  ", '" + client->authInfo->email + "', '" + client->authInfo->passwd + "'"
	  ", '" + client->baseInfo->FirstName + "', '" + client->baseInfo->Patronymic + "', '" + client->baseInfo->LastName + "'"
    ", '" + client->baseInfo->phone +
	  ");";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("Reg");
    code = ns1__responseCodeType__Failure;
    return SOAP_OK;
  }
  code = ns1__responseCodeType__Success;
  return SOAP_OK;
}

int OrdersManagerBindingService::Authorization(ns1__clientAuthorizationInfoType *auth, ns1__responseCodeType &code)
{
  if (!check_email(auth->email))
  {
    code = ns1__responseCodeType__EmailFailure;
    return SOAP_OK;
  }
  if (!check_password(auth->email, auth->passwd))
  {
    code = ns1__responseCodeType__PasswordFailure;
    return SOAP_OK;
  }
  code = ns1__responseCodeType__Success;
  return SOAP_OK;
}

int OrdersManagerBindingService::SendOrder(ns1__OrderType *order, ns1__clientAuthorizationInfoType *auth, ns1__responseCodeType &code)
{
  if (!check_password(auth->email, auth->passwd))
  {
    code = ns1__responseCodeType__PasswordFailure;
    return SOAP_OK;
  }

  std::string query = "INSERT INTO `omserver`.`orders` "
    "(`id`, `idOwner`, `idDestination`, `idBusiness`, `idDoer`, `Header`, `Description`, `Price`, `Deadline`) "
    "VALUES (NULL"
    ", " + db_value_id(order->idOwner) +
    ", " + db_value_id(order->idDestination) +
    ", " + db_value_id(order->idBusiness) +
    ", " + db_value_id(order->idDoer) +
    ", '" + order->Header + "'"
    ", '" + order->Description + "'"
    ", '" + order->Price + "'"
    ", '" + order->Deadline + "'"
    ");";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("SendOrder");
    code = ns1__responseCodeType__Failure;
    return SOAP_OK;
  }
  code = ns1__responseCodeType__Success;
  return SOAP_OK;
}

int OrdersManagerBindingService::TakeOrder(int id, ns1__clientAuthorizationInfoType *auth, enum ns1__responseCodeType &code)
{
  if (!check_password(auth->email, auth->passwd))
  {
    code = ns1__responseCodeType__PasswordFailure;
    return SOAP_OK;
  }  

  std::string query = "UPDATE `omserver`.`orders` "
    "SET "
    "`idDoer` = " + db_value_id(UserId(auth->email)) +
    " WHERE `orders`.`id` =" + db_value_id(id) + ";";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("TakeOrder");
    code = ns1__responseCodeType__Failure;
    return SOAP_OK;
  }
  code = ns1__responseCodeType__Success;
  return SOAP_OK;
}

int OrdersManagerBindingService::FinishOrder(int id, ns1__clientAuthorizationInfoType *auth, enum ns1__responseCodeType &code)
{
  if (!check_password(auth->email, auth->passwd))
  {
    code = ns1__responseCodeType__PasswordFailure;
    return SOAP_OK;
  }

  std::string query = "UPDATE `omserver`.`orders` "
    "SET "
    "`isFinished` = 1"
    " WHERE `orders`.`id` =" + db_value_id(id) + ";";
  if (mysql_query(db, query.c_str()))
  {
    db_error_handler("TakeOrder");
    code = ns1__responseCodeType__Failure;
    return SOAP_OK;
  }
  code = ns1__responseCodeType__Success;
  return SOAP_OK;
}

static std::string build_query(ns1__OrdersQueryType *Query)
{
  std::string query = 
  "SELECT `id`, `Header`, `idOwner`, `idDestination`, `idBusiness`, `Price`, `Deadline`, `Description`, `idDoer`, `isFinished`"
  " FROM `omserver`.`orders`"
  " WHERE";
  if (Query->idDoer == -1)
    query += " `idDoer` IS NULL";
  else if (Query->idDoer >= 0)
  {
    query += " `idDoer` = "
        + std::to_string(Query->idDoer)
        + " AND `isFinished` = " + std::to_string(Query->isFinished);
  }
  query += " ORDER BY `id` DESC;";
  return query;
}

int OrdersManagerBindingService::FindOrders(ns1__OrdersQueryType *Query, ns1__clientAuthorizationInfoType *auth, struct __ns1__FindOrdersResponse &resp)
{
  if (!check_password(auth->email, auth->passwd))
  {
    resp.code = ns1__responseCodeType__PasswordFailure;
    return SOAP_OK;
  }  
  if (mysql_query(db, build_query(Query).c_str()))
  {
    db_error_handler("FindOrders: query");
    resp.code = ns1__responseCodeType__Failure;
    return SOAP_OK;
  }
  MYSQL_RES *resStore = mysql_store_result(db);
  if (resStore == NULL)
  {
    db_error_handler("FindOrders: store");
    return false;
  }

  clearOrdersList();
  resp.orders = &OrdersList;

  MYSQL_ROW row;
  while(row = mysql_fetch_row(resStore))
  {
    ns1__OrderType* order = newOrder();
    db_field_id(row[0], &order->id);
    db_field_string(row[1], &order->Header);
    db_field_id(row[2], &order->idOwner);
    db_field_id(row[3], &order->idDestination);
    db_field_id(row[4], &order->idBusiness);
    db_field_string(row[5], &order->Price);
    db_field_string(row[6], &order->Deadline);
    db_field_string(row[7], &order->Description);
    db_field_id(row[8], &order->idDoer);
    db_field_bool(row[9], &order->isFinished);
  }
  
  mysql_free_result(resStore);
  
  resp.code = ns1__responseCodeType__Success;
  return SOAP_OK;
}
